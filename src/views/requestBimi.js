//封装了axios工具类
import axios from 'axios'

const bimAxios = axios.create({

})

// request interceptor
bimAxios.interceptors.request.use(
    //访问bim服务器必须在http header中增加Authorization=bimi，这样可以防止网络爬虫攻击
    config => {
        config.headers['Authorization']='bimi';
        return config
    }, 
    error => {
        console.log(error) 
        Promise.reject(error)
    }
)

// respone interceptor
bimAxios.interceptors.response.use(
    response => {
        const data = response.data;
        return data;
    },
    error => {
        return Promise.reject(error)
    })

export default bimAxios;